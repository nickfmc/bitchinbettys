<?php
/*
Template Name: Portfolio Listing Page
*/
?>
  <?php get_header(); ?>


  <main class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section class="EntryContent  cf">
      <!-- <h1 class="MainTitle"><?php the_title(); ?></h1> -->
      <?php the_content(); ?>

    </section>
    <!-- /EntryContent -->
    <?php endwhile; endif; // END main loop (if/while) ?>

    <div class="Strip">
    <div class="SectionContainer">
      <!-- Get Isotope Filters -->
      <div class="button-group filter-button-group">
      <button class="button is-checked" data-filter="*">All</button>
        <?php
  $terms = get_terms( 'portcat_tax' );
  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
    
    foreach ( $terms as $term ) {
      echo '<button data-filter=".' . $term->slug . '">' . $term->name . '</button>';
    }

  };
?>
      </div>

      <div class="PortfolioItems grid">
      <div class="grid-sizer"></div>
        <?php
  $args = array(
    'posts_per_page' => -1,
    'post_type' => 'portfolio_type',
    'orderby' => 'menu_order',
    'order' => 'asc'
  );
  $cpt_query = new WP_Query($args);
?>
          <?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>


          <div class="PortfolioItem  element-item  <?php
              $terms = get_the_terms( $post->ID, 'portcat_tax' );
              if ($terms) {
                foreach( $terms as $term ) {
                  echo $term->slug;
                }
              }
          ?>">
          <?php the_post_thumbnail('large');?>
          <div class="PortfolioItem-overlay">
          <a href="<?php the_permalink(); ?>"></a>
            <div class="PortfolioItem-text">
             
             <?php the_title(); ?>
            </div>
          </div>

          </div>
          <!-- /CLASS-NAME-HERE -->

          <?php endwhile; endif; // end of CPT loop ?>
          <?php wp_reset_postdata(); ?>

      </div>

    </div>
    </div>



    <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

  </main>


  <?php get_footer(); ?>