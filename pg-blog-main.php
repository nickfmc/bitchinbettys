<?php
/*
Template Name: Blog Main Page
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<div class="BlogBanner  u-verticalCenter" style="background-image: url(<?php if (current_theme_supports( 'post-thumbnails' ) && has_post_thumbnail( $post->ID )) {
  $page_bg_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  $page_bg_image_url = $page_bg_image[0]; // this returns just the URL of the image
  echo $page_bg_image_url;
} ?> );"> 
  <h1 class="">Blog</h1>
</div>
       
          <?php the_content(); ?>

      <?php endwhile; endif; // END main loop (if/while) ?>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
      <div class="PrimaryContent">

        <?php
          $temp = $wp_query;
          $wp_query = null;
          $wp_query = new WP_Query(
            array(
                'posts_per_page' => 6,
                'paged' => $paged
              )
            );
        ?>

        <?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

          <article <?php post_class('BlogIntroWrap'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

            <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
                <div class="BlogContent-image"><?php the_post_thumbnail('crop-800-450'); ?></div>
              
              <div class="BlogContent-content">
                <h2 itemprop="headline"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                <div class="EntryMeta">
                    <!-- <span class="EntryMeta-author" itemprop="author" itemscope itemptype="http://schema.org/Person">By: <?php echo get_the_author(); ?> | </span> -->
                    <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>
                </div> <!-- /EntryMeta -->
                <?php the_excerpt(); ?>
   
              </div>

            </section> <!-- /EntryContent -->

          </article> <!-- /article -->

        <?php endwhile; endif; ?>

        <?php /* Display navigation to next/previous pages when applicable */ ?>
        <?php if ( $wp_query->max_num_pages > 1 ) : ?>
          <?php $max_page = $wp_query->max_num_pages; ?>
          <nav class="PostNav">
            <ul class="cf">
              <li class="PostNav-prev"><?php next_posts_link(__('&laquo; Older Entries', 'flexdev')) ?></li>
               <?php if (($paged < $max_page) && ($paged > 1))  { echo "<li class='nav-divider'><span>|</span></li>"; }  ?>
              <li class="PostNav-next"><?php previous_posts_link(__('Newer Entries &raquo;', 'flexdev')) ?></li>
            </ul>
          </nav>
        <?php endif; ?>

        <?php $wp_query = null; $wp_query = $temp; ?>
        <?php wp_reset_postdata(); ?>

      </div> <!-- /PrimaryContent -->

      <?php get_sidebar(); // sidebar ?>

    </main>
  </div> <!-- /Strip -->

<?php get_footer(); ?>
