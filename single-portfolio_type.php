<?php get_header(); ?>


<main class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <section class="EntryContent  cf">
    <!-- <h1 class="MainTitle"><?php the_title(); ?></h1> -->
    <?php the_content(); ?>
  </section>
  <!-- /EntryContent -->
  <?php endwhile; endif; // END main loop (if/while) ?>

  <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>


  <div class="Strip Strip--matchBB">
  <div class="SectionContainer">
  
   <div class="PortfolioShare" style="margin-bottom:60px;">
     Share This Story:
    <!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32">
      <a class="a2a_button_facebook">
      <svg class="icon icon-facebook"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-facebook"></use></svg>
      </a>
      <a class="a2a_button_twitter">
      <svg class="icon icon-twitter"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-twitter"></use></svg>
      </a>
      <a class="a2a_button_google_plus">
      <svg class="icon icon-google-plus"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-google-plus"></use></svg>
      </a>
      <a class="a2a_button_pinterest">
      <svg class="icon icon-pinterest-p"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-pinterest-p"></use></svg>
      </a>
      <a class="a2a_button_email">
        <svg class="icon icon-envelope-o"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-envelope-o"></use></svg>
      </a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->
  </div>
 

  <a class="AllItemLink" href="/video-portfolio">View All Portfolio Items</a>
  </div>
  </div>


</main>


<?php get_footer(); ?>