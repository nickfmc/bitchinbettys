<div id="ModalNav" class="ModalNav">
  <div class="close-ModalNav  CloseModalNavButton">
    <!-- class name must match id above, ie. close-IdName -->
    <svg class="icon icon-close">
      <use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-close"></use>
    </svg>
  </div>

  <nav class="ModalContactBlocks">
    <ul>
      <li class="ModalContactBlock">
        <a href="/" rel="nofollow">
          <svg class="icon icon-home">
            <use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-home"></use>
          </svg>
        </a>
      </li>
      <li class="ModalContactBlock">
        <a href="mailto:info@redwagonvideo.com">
          <svg class="icon icon-envelope-o">
            <use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-envelope-o"></use>
          </svg>
        </a>
      </li>
      <li class="ModalContactBlock">
        <a href="tel:2508082807">
          <svg class="icon icon-phone">
            <use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-phone"></use>
          </svg>
        </a>
      </li> 
      <li class="ModalContactBlock">
        <a target="_blank" href="https://www.youtube.com/channel/UCMqV_z6UagAPL2KtjgoRJAg">
          <svg class="icon icon-youtube">
            <use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-youtube"></use>
          </svg>
        </a>

      </li>
      <li class="ModalContactBlock">
        <a target="_blank" href="https://vimeo.com/user41200952">
          <svg class="icon icon-vimeo">
            <use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-vimeo"></use>
          </svg>
        </a>
      </li>
    </ul>
  </nav>

  <div class="ModalNavWrap">
    <img class="MobileMenuLogo" src="<?php bloginfo('template_url') ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>"
    />
    <nav class="MobileMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

      <ul class="MobileMenu">
        <?php
  $pageargs=array(
    'depth' => 2,
    'title_li' => '',
    'include_tree' => '6,9',
    'sort_column'=> 'menu_order',
    'sort_order'=> 'asc',
    'walker'=> new FDT_Arrow_Walker_List_Pages()
    );
    ub_wp_list_pages($pageargs);
?>



          <li class="page_item page-item-11 page_item_has_children parent-menu-item">
            <a class="ExpandMenuLink" ref="#">Mission Briefs</a>
            <span class="ExpandMenuBtn">
              <span></span>
            </span>
            <ul class="SubMenu">
              <?php fdt_nav_menu( 'services-nav', 'menu-services' ); ?>
            </ul>
          </li>

          <li class="page_item page-item-283 page_item_has_children parent-menu-item">
            <a href="/blog/">Blog</a>
          </li>

          <li class="page_item page-item-10 page_item_has_children parent-menu-item">
            <a class="ExpandMenuLink" href="#">Contact Us</a>
            <span class="ExpandMenuBtn">
              <span></span>
            </span>
            <ul class="SubMenu">
              <?php fdt_nav_menu( 'about-nav', 'menu-about' ); ?>
            </ul>
          </li>

      </ul>



    </nav>
  </div>
  <!-- /ModalNavWrap -->
</div>
<!-- /ModalNav -->