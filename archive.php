<?php get_header(); ?>

<div class="BlogBanner  u-verticalCenter" style="background-image: url(<?php if (current_theme_supports( 'post-thumbnails' ) && has_post_thumbnail( '283' )) {
  $page_bg_image = wp_get_attachment_image_src( get_post_thumbnail_id( '283' ), 'full' );
  $page_bg_image_url = $page_bg_image[0]; // this returns just the URL of the image
  echo $page_bg_image_url;
} ?> );">
  <?php
          the_archive_title( '<h1 class="MainTitle">', '</h1>' );
          the_archive_description( '<div class="taxonomy-description">', '</div>' );
        ?>
</div>

<div class="Strip">

  <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

    <div class="PrimaryContent">
      <a href="/blog" style="margin-top:20px; display:inline-block;">
        << Back to Blog</a>
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <article <?php post_class( 'BlogIntroWrap'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

            <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
              <div class="BlogContent-image">
                <?php the_post_thumbnail('crop-800-450'); ?>
              </div>

              <div class="BlogContent-content">
                <h2 itemprop="headline">
                  <a href="<?php the_permalink() ?>">
                    <?php the_title(); ?>
                  </a>
                </h2>
                <div class="EntryMeta">
                  <!-- <span class="EntryMeta-author" itemprop="author" itemscope itemptype="http://schema.org/Person">By: <?php echo get_the_author(); ?> | </span> -->
                  <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>
                </div>
                <!-- /EntryMeta -->
                <?php the_excerpt(); ?>

              </div>

            </section>
            <!-- /EntryContent -->

          </article>
          <!-- /article -->

          <?php endwhile; ?>

          <nav class="PostNav">
            <ul class="cf">
              <li class="PostNav-prev">
                <?php next_posts_link(__('&laquo; Older Entries', 'flexdev')) ?>
              </li>
              <li class="PostNav-next">
                <?php previous_posts_link(__('Newer Entries &raquo;', 'flexdev')) ?>
              </li>
            </ul>
          </nav>
          <!-- /PostNav -->

          <?php else : ?>

          <article class="PostNotFound">
            <header class="ArticleHeader">
              <h2>
                <?php _e("Oops, Post Not Found!", "flexdev"); ?>
              </h2>
            </header>
            <section class="EntryContent">
              <p>
                <?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?>
              </p>
            </section>
            <footer class="ArticleFooter">
              <p>
                <?php _e("This is the error message in the archive.php template.", "flexdev"); ?>
              </p>
            </footer>
          </article>

          <?php endif; ?>

    </div>
    <!-- /PrimaryContent -->

    <?php get_sidebar(); // sidebar ?>

  </main>
</div>
<!-- /Strip-->

<?php get_footer(); ?>
