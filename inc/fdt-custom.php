<?php
/**
 * Any small custom functions for this project? If yes, drop them here
 */


// *************** DETETERMINE IF A PAGE IS WITHIN A TREE ********************
function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
  global $post;          // load details about this page
  if ( is_page($pid) )
    return true;        // we're at the page or at a sub page
  $anc = get_post_ancestors( $post->ID );
  foreach ( $anc as $ancestor ) {
    if( is_page() && $ancestor == $pid ) {
      return true;
    }
  }
  return false;          // we aren't at the page, and the page is not an ancestor
}

/**
 * ub_wp_list_pages function
 *
 * Builds include and exclude parameters from a tree before calling
 * a standard wp_list_pages call. All parameters are the same as the
 * wp_list_pages functions with one exception listed below.
 *
 * @param string $args[ 'include_tree' ] Define a comma-separated list of Parent IDs to be included
 */
function ub_wp_list_pages( $args ) {
  
    /**
     * Set default includes and excludes since these are the only parameters
     * we are editing with this function. Set echo since it will be used at the
     * end of the function
     */
    $includes_excludes = array(
      'echo'         => true,
      'exclude'      => '',
      'exclude_tree' => '',
      'include'      => '',
      'include_tree' => '',
    );
  
    // parse and merge args with defaults
    $args = wp_parse_args( $args, $includes_excludes );
  
    /**
     * Get page IDs and children from include and exclude trees and append them to the standard include
     * and exclude arguments
     */
    $args[ 'exclude' ] = ub_wp_list_pages_parse_tree( $args[ 'exclude'], $args[ 'exclude_tree' ] );
    $args[ 'include' ] = ub_wp_list_pages_parse_tree( $args[ 'include'], $args[ 'include_tree' ] );
  
    /**
     * Unset the include and exclude trees (we are done with them)
     */
    unset( $args[ 'exclude_tree' ] );
    unset( $args[ 'include_tree' ] );
  
    /**
     * Call wp_list_pages or return the value of wp_list_pages depending on the echo parameter
     */
    if ( $args[ 'echo' ] )
      wp_list_pages( $args );
    else
      return wp_list_pages( $args );
  }
  
  /**
   * ub_wp_list_pages_parse_tree function
   *
   * Helper function for ub_wp_list_pages. Generates a comma-separated list for the include or exclude
   * parameters based on the given value of that parameter and the value of the related tree.
   *
   * @param string $list The comma-separated list of includes or excludes
   * @param string $tree The comma-separated list of include_tree or exclude_tree parent IDs
   * @return string $list The final comma-separated list of includes or excludes
   */
  function ub_wp_list_pages_parse_tree( $list = '', $tree = '' ) {
  
    if ( ! $tree )
      return $list; // return default or given list value if no tree to parse
  
    $list = explode( ',', $list ); // turn $list into an array so we can easily add to it later
    $tree_array = $tree = explode( ',', $tree ); // turn $tree into an array so we can iterate through it
  
    /**
     * Iterate through the $tree array to get the children of each page in the $tree. Add the IDs of each
     * child into $tree_array to merge into the $list
     */
    foreach ( $tree as $parent ) {
  
      if ( ! ( $child_pages = get_pages( array( 'child_of' => $parent ) ) ) )
        continue; // skip processing if no child pages found for the current parent
  
      foreach ( $child_pages as $child_page )
        $tree_array[] = $child_page->ID;
  
    }
  
    $list = array_merge( $list, $tree_array );
  
    return implode( ',', $list );
  }

// ************** CHECK IF PAGE HAS PARENT ****************
function page_has_parent() {
  global $post;

  $children = get_pages('child_of='.$post->ID);
  if( count( $children ) > 0 ) {
      $parent = true;
  }

  return $parent;
}

// ************** Mobile nav walker ****************
class FDT_Arrow_Walker_List_Pages extends Walker_Page {
  function start_lvl( &$output, $depth=0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .='<span class="ExpandMenuBtn"><span></span></span>';
    $output .= "\n$indent<ul class=\"SubMenu\">\n";
  }
}

class FDT_Arrow_Walker_Nav_Menu extends Walker_Nav_Menu {
  function start_lvl( &$output, $depth=0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .='<span class="ExpandMenuBtn"><span></span></span>';
    $output .= "\n$indent<ul class=\"SubMenu\">\n";
  }
}

// ************ CUSTOM TITLE LENGTH *********************
/* usage: <?php echo title_crop(55); ?> */
function title_crop($count) {
  $title = get_the_title();
  if (strlen($title) > $count) {
    $title = substr($title, 0, $count) . '...';
  }
  return $title;
}


add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


?>
