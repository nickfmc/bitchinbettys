<?php
/**
 * Using Shortcodes? Good on ya. Let's setup them up here.
 */


 /*********** SHORTCODE RELATED CLEAN UP ********************/

 // STOP SHORTCODES THAT DON'T USE INLINE CONTENT FROM BEING WRAPPED IN A P TAG (until WP fixes this)
 // ** HEY!! -> ensure you change the array to the shortcodes you are using!
 add_filter('the_content', 'fdt_content_filter');
 function fdt_content_filter($content) {
   // array of custom shortcodes requiring the fix
   $block = join("|",array( 'blockquote' ));
   // opening tag
   $rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
   // closing tag
   $rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
   return $rep;
 }


 // ALLOW SHORTCODES IN TEXT WIDGETS & STOP IT BEING WRAPPED IN A P TAG IN THE WIDGET
 add_filter( 'widget_text', 'shortcode_unautop');
 add_filter( 'widget_text', 'do_shortcode', 11);




// **** Intro P tag shortcode
function fdt_featured_text($atts, $content=null, $code="") {
  $return = '<p class="FeaturedText">';
  $return .= $content;
  $return .= '</p>';
  return $return;
}
add_shortcode('featured-text' , 'fdt_featured_text' );


// **** Feature Button shortcode
function fdt_link_button($atts, $content=null, $code="") {
  extract(shortcode_atts(array(
    'color' => 'blue',
  ), $atts));
  return '<div class="LinkButton  LinkButton--'. $color . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('button', 'fdt_link_button');


// **** Vimeo shortcode
function vimeo_popup($atts, $content=null, $code="") {
  extract(shortcode_atts(array(
    'id' => '',
  ), $atts));
  return '<div class="vimeo-pop" data-video-id="'. $id . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('vimeo', 'vimeo_popup');


// **** Blockquote shortcode
function fdt_bquote($atts, $content = null) {
  extract(shortcode_atts(array(
    'cite' => 'Unknown',
    'url' => 'url'
  ), $atts));
  if ( $cite == 'Unknown' ) {
    $return = '<blockquote>'. do_shortcode($content) .'</blockquote>';
  } elseif ( $url == 'url' ) {
    $return = '<blockquote>'. do_shortcode($content) .'<p>'. $cite .'</p></blockquote>';
  } else {
    $return = '<blockquote><p>'. do_shortcode($content) .'</p><p><a href="'. $url .'">'. $cite .'</a></p></blockquote>';
  }
  return $return;
}
add_shortcode('blockquote', 'fdt_bquote');




?>
