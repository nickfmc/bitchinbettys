<?php get_header(); ?>

<!-- <div class="Strip  Strip--BlogTitle">
  <h1 itemprop="headline"><?php the_title(); ?></h1>
</div> -->

  <div class="Strip ">
    <main class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <div class="PrimaryContent">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <article <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

            <header class="ArticleHeader">

             

           

            </header> <!-- /ArticleHeader -->

            <section class="EntryContent  SingleBlogContent  cf" itemprop="articleBody">
              <?php the_content(); ?>
            </section> <!-- /EntryContent -->

            <footer class="ArticleFooter SectionContainer">

            <div class="EntryMeta">
                <span class="EntryMeta-author" itemprop="author" itemscope itemptype="http://schema.org/Person">By: <?php echo get_the_author(); ?> | </span>
                <span>Categories: <?php the_category(', '); ?></span>
            </div> <!-- /EntryMeta -->

            <div class="PostShare">
     Share This Post:
    <!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32">
      <a class="a2a_button_facebook">
      <svg class="icon icon-facebook"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-facebook"></use></svg>
      </a>
      <a class="a2a_button_twitter">
      <svg class="icon icon-twitter"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-twitter"></use></svg>
      </a>
      <a class="a2a_button_google_plus">
      <svg class="icon icon-google-plus"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-google-plus"></use></svg>
      </a>
      <a class="a2a_button_pinterest">
      <svg class="icon icon-pinterest-p"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-pinterest-p"></use></svg>
      </a>
      <a class="a2a_button_email">
        <svg class="icon icon-envelope-o"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-envelope-o"></use></svg>
      </a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->
  </div>


            </footer> <!-- /article footer -->

           

          </article> <!-- /article -->

        <?php endwhile; else : ?>

          <article class="PostNotFound">
            <header class="ArticleHeader">
              <h1><?php _e("Oops, Post Not Found!", "flexdev"); ?></h1>
            </header>
            <section class="EntryContent">
              <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
            </section>
            <footer class="ArticleFooter">
              <p><?php _e("This is the error message in the single.php template.", "flexdev"); ?></p>
            </footer>
          </article>

        <?php endif; ?>

      </div> <!-- /PrimaryContent -->

      <?php // get_sidebar(); // sidebar ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>
