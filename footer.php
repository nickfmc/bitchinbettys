<footer class=" PageFooter" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
  <div class="Strip  Footer-upper">
    <div class="SectionContainer" style="text-align:center;">

    <div class="LinkButton  LinkButton--blue"><a href="/contact-us/">Contact us</a></div>



    </div>
    <!-- /container-->
  </div>
  <div class="Strip  Footer-lower">
    <div class="SectionContainer">
      <span class="SiteCopyright">Bitchin Bettys © 2021</span>
    </div>
  </div>
</footer>
<!-- /footer -->

<?php get_template_part( 'parts/modal-menu' ); ?>

<!-- all js scripts are loaded in lib/fdt-enqueues.php -->
<?php wp_footer(); ?>

</body>

</html>
<!-- end page. what a ride! -->