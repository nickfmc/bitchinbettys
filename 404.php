<?php get_header(); ?>

  <div class="Strip">
    <main class="SectionContainer" style="padding:15rem 2rem 0 2rem; text-align:center; height: 80vh;" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
      <h1>Sorry - This Page Does Not Exist! (404)</h1>
      <section class="EntryContent">
        <h4>The page you were looking for was not found. <br />Please try going to the <a href='/'>Home Page</a>.</h4>
      </section> <!-- /EntryContent -->
    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>
