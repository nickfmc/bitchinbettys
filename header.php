<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php // mobile meta (hooray!) ?>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <?php // icons & favicons ?>
  <?php // Using REAL FAVICON GENERATOR? Put generated code in file below + uncomment admin favicon setup (in admin.php) ?>
  <?php // get_template_part( 'favicon-head' ); ?>

  <?php // other html head stuff (before WP/theme scripts are loaded) ?>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet"> 

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <?php // drop Google Analytics here ?>
  <?php // end Analytics ?>

</head>

<body <?php body_class(pretty_body_class()); ?> itemscope itemtype="http://schema.org/WebPage">
<!--[if lt IE 11]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->

  <header class="PageHeader HeaderTop" id="PageHeader" role="banner" itemscope itemtype="http://schema.org/WPHeader">


  <div class="Strip  PageHeader-main">
    <div class="SectionContainer">

      <div class="SiteLogo" itemscope itemtype="http://schema.org/Organization">
        <a href="/" rel="nofollow">
          <img src="<?php bloginfo('template_url') ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>" />
        </a>
      </div>

      <a href="#ModalNav" id="ModalMenuButton" class="ModalMenuButton">
        <span>Menu</span>
        <svg class="icon icon-menu"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-menu"></use></svg>
      </a>
      <!-- <div class="LinkButton">
       <a href="#">Start a Video Project</a>
      </div> -->
      <nav id="site-navigation" class="c-main-navigation" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">
        <?php fdt_nav_menu( 'main-nav', 'c-main-menu' ); // Adjust using Menus in WordPress Admin ?>
      </nav>
    </div> <!-- /SectionContainer -->

   

    <div class="NavSlant">
        
      </div>

    </div>

  </header> <!-- /PageHeader -->
