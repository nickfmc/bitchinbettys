/**
 * FlexDev Theme Scripts File **************
 * This file contains any js scripts you want added to the theme. Instead of calling it in the header or throwing
 * it inside wp_head() this file will be called automatically in the footer so as not to slow the page load.
 */

jQuery(window).load(function () {
  jQuery('.video-bg-noloop video').removeAttr('loop');
});

/* JQUERY VIEWPORT RE-SIZING? --> SEE HELPERS/RESPONSIVE MISC */


jQuery(document).ready(function ($) {
// *********************** START CUSTOM SCRIPTS *******************************


  // modal menu init
  var modal_menu = jQuery("#ModalMenuButton").animatedModal({
    modalTarget: 'ModalNav',
    animatedIn: 'slideInDown',
    animatedOut: 'slideOutUp',
    animationDuration: '0.50s',
    color: '#282A2B'
  });
  // close modal menu if esc key is used
  jQuery(document).keyup(function(ev){
    if(ev.keyCode == 27) {
      modal_menu.close();
    }
  });

  $('.ModalMenuButton').on("click", function(){
    $('.ModalContactBlocks').addClass('visible');
    $('.CloseModalNavButton').addClass('visible');
  });
  // accordion for modal menu navigation -------------------------
  $('.ExpandMenuBtn').on( "click", function() {
    $( this ).next().toggleClass('is-selected');
    $( this ).toggleClass('activeArrow');
    });

    $('.ExpandMenuLink').on( "click", function() {
      $( this ).next().next().toggleClass('is-selected');
      $( this ).next().toggleClass('activeArrow');
      });


// Modal Video
//$(".js-modal-btn a").modalVideo();
$(".vimeo-pop").modalVideo({channel:'vimeo'});

  // objectfit polyfill
  $(function () { objectFitImages('.u-polyFit'); });

  /**
  * HEADROOM - SCROLL UP BAR NAVIGATION
  */

var headerheight =  $('.HeaderTop').height();
  $(".HeaderTop").headroom({
    "offset": headerheight,
    "tolerance": 5,
    "classes": {
      "initial": "animated",
      "pinned": "slideInDown",
      "unpinned": "slideOutUp"
    }
  });


 var homeheaderheight =  $('.VideoHPBanner').height();
  $(".HeaderBottom").headroom({
    "offset": homeheaderheight,
    "tolerance": 5,
    "classes": {
      "initial": "animated",
      "pinned": "slideInDown",
      "unpinned": "slideOutUp"
    }
  });


 

  var divWidth = $('.element-item').width(); 
  $('.element-item').height(divWidth);

  // init Isotope
var $grid = $('.PortfolioItems').isotope({
  // options
  masonry: {
    columnWidth: '.grid-sizer',
    gutter: 10
  }
});
// filter items on button click
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

$( ".VideoHPBanner" ).append( "<div class='VideoTLow'></div>" );
$( ".VideoHPBanner" ).append( "<div class='VideoTHigh'></div>" );


// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});




// *********************** END CUSTOM SCRIPTS *********************************

}); /* end of DOM ready scripts */

jQuery(window).resize(function(){
  jQuery('.element-item').height(divWidth);
});